# escape=\ (backslash)
# Staticsfc
#
# VERSION               0.0.1
# AUTHOR                Eduardo de Andrade
# EMAIL                 duzackzack@gmail.com

FROM    ubuntu
VOLUME /tmp

EXPOSE 8080
LABEL Description="Esta imagem tem por objetivo iniciar o servidor do staticsfc" Vendor="EANDRADE Products" Version="1.0"
RUN apt-get update && apt-get install -y inotify-tools apache2 openssh-server git
RUN apt-get clean all
RUN apt-get install -y default-jre
RUN apt-get install -y default-jdk
RUN apt-get install -y openjdk-8-jre
RUN apt-get install -y openjdk-8-jdk
RUN apt-get install -y software-properties-common && add-apt-repository ppa:maxmind/ppa && apt-get update && apt-get -y install libmaxminddb0 libmaxminddb-dev mmdb-bin
RUN apt-get clean all
RUN apt-get update
RUN apt-get install -y iptables
RUN apt-get install -y sudo
RUN apt-get install net-tools
ADD  http://ftp.unicamp.br/pub/apache/tomcat/tomcat-8/v8.5.38/bin/apache-tomcat-8.5.38.tar.gz /tmp/

COPY extrair.sh /tmp/extrair.sh
RUN ./tmp/extrair.sh


