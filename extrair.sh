#!/bin/bash

cd /tmp/
echo "baixando servidor"
wget http://ftp.unicamp.br/pub/apache/tomcat/tomcat-8/v8.5.53/bin/apache-tomcat-8.5.53.tar.gz

echo "download ok"

echo "*******************"
tar  -zxvf apache-tomcat-8.5.53.tar.gz


mv apache-tomcat-8.5.53 /usr/local/tomcat8

cd /

rm -rf /usr/local/tomcat8/webapps/ROOT*

 /usr/local/tomcat8/bin/./shutdown.sh

echo "Iniciando o clone do WAR"
cd /

cd tmp/
git clone https://eduardo_de_andrade@bitbucket.org/eduardo_de_andrade/build.git
echo "Fim do Clone"

cd build/

echo "iniciando deploy aplicacao ROOT"
cp ROOT.war /usr/local/tomcat8/webapps/
echo "fim da aplicacao ROOT"
echo "......................"
echo "Inicio deploy aplicacao mobile"
cp mobile.war  /usr/local/tomcat8/webapps/
echo "fim deploy mobile"
echo "......."
echo "inicio micro-servico webzl-videos"
cp webzl-videos.war /usr/local/tomcat8/webapps/
echo "Fim fim deploy micro-servico"

echo "fim deploy"

echo "iniciando servidor"
/usr/local/tomcat8/bin
./startup.sh

cd ../logs
tail -f catalina.out
